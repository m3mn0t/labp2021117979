import random
import time

import pandas as pd
from requests import get
from io import StringIO
from csv import DictReader
import matplotlib.pyplot as plt
import seaborn as sns
import pandas


def constructitlines():
    url = 'https://raw.githubusercontent.com/dssg-pt/covid19pt-data/master/data_concelhos_new.csv'
    req = get(url, stream=True)
    buffer = StringIO(req . text)
    return DictReader(buffer, delimiter=',')

dicionario_incidencia = []
dicionario_population_85_mais = []


def incidencia():
    a = 0
    for key in constructitlines():
        if a != 10:
            dicionario_incidencia.append(key["incidencia"])
            a += 1


def pop85():
    b = 0
    for key in constructitlines():
        if b != 10:
            dicionario_population_85_mais.append(key["population_85_mais"])
            b += 1

incidencia()

pop85()
andre = pd.DataFrame(dicionario_incidencia, columns=['Incidencia'])


print(andre)
