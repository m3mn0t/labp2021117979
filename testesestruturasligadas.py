from node import Node

head = None
new_node = Node(1)
head = new_node
#print(head)
new_node = Node(2, head)
head = new_node
#print(head)
new_node = Node(3, head)
head = new_node
#print(head)

#B
head = None
for i in range(1, 10):
    head = Node(i, head)

#print(head)

#c
def visualizar_estrutura_ligada(head):
    aux = head
    while aux is not None:
        print(aux.data)
        aux = aux.next
    print("-------")
    print(head)

#d
head = Node(10, head) #adicionar no inicio
aux = head
while aux.next is not None:
    aux = aux.next
aux.next = Node(0)
print(aux.next)
visualizar_estrutura_ligada(head)

#e

#f
head = head.next
print(" ++++++ ")
visualizar_estrutura_ligada(head)

aux = head
while aux.next.next is not None:
    aux = aux.next
aux.next = None

#h
counter = 0
aux = head
while aux is not None:
    counter += 1
    aux = aux.next
print(" numero de elementos", counter)