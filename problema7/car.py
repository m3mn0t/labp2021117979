class Car:
    def __init__(self, id, state, fuel, fuel_threshold, waiting_time_threshold):
        """
        inicia o estado do automóvel
        """
        self.__id = id
        self.__state = state
        self.__fuel = fuel
        self.__fuel_threshold = fuel_threshold
        self.__waiting_time_threshold = waiting_time_threshold
        self.__nr_times_station = 0
        self.__waiting_time = 0

    def reset_waiting_time(self):
        """
        leva para zero o valor de espera

        :return:
        """
        self.__waiting_time = 0

    def increment_waiting_time(self, value):
        """
        Incrementa o tempo de espera
        :param value:
        :return:
        """

        self.__waiting_time += value

    def get_waiting_time(self):
        """
        Retorna o tempo de espera
        :return:
        """
        return self.__waiting_time

    def get_waiting_time_threshold(self):
        '''retorna o tempo limite de espera na fila de combustível'''
        return self.__waiting_time_threshold

    def get_id(self):
        '''retorna o id do automóvel'''
        return self.__id

    def get_state(self):
        '''retorna o estado'''
        return self.__state

    def set_state(self, state):
        '''atribuí um estado ao automóvel'''
        self.__state = state

    def reduce_fuel(self, value):
        '''reduz o nível de combustível por um valor value'''
        self.__fuel -= value

    def get_fuel(self):
        """
        Retorna o nível de combustível
        :return:
        """
        return self.__fuel

    def set_fuel(self, fuel):
        """
        Atribui um valor ao nível de combustível
        """
        self.__fuel = fuel

    def get_fuel_threshold(self):
        """
        Retorna o limite mínimo de combustível
        :return:
        """
        return self.__fuel_threshold

    def increment_nr_times_station(self):
        """
        incrementa o número de vez que usou o posto de combustível
        """
        self.__nr_times_station += 1

    def get_nr_times_station(self):
        return self.__nr_times_station

