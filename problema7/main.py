from car import Car
import random
import doctest

# Global variables

MAX_TICKS = 10
NR_CARS = 5
MAX_FUEL = 10

cars_stopped = []
cars_onroad = []
cars_queue = []


# Ensure the number of cars created is always bigger than 5.
def create_dict_cars(dict_cars,nr_cars:int, randfuel:tuple,randfuel_T:tuple,randtime_T:tuple, cars_onroad:list)->int:
    """
    Keep values in the dictionary and return the number of cars created (length of the dictionary)
    :param nr_cars:
    :param randfuel:
    :param randfuel_T: (a,b) tuple to calculate random integer values between a and b for fuel_threshold.
    :param randtime_T: (a,b) tuple to calculate random integer values betwwen a and b for waiting_time_threshold.
    :param cars_onroad:
    :return:
    """

    """
    >>> create_dict_cars({}, 3, (5, 7), (2, 5), (2, 4), [])
    6
    """
    state = "onroad"
    if nr_cars <= 5:
        nr_cars = 6

    for id in range(nr_cars):
        fuel = random.randint(randfuel[0],randfuel[1])
        fuel_threshold= random.randint(randfuel_T[0],randfuel_T[1])
        waiting_time_threshold = random.randint(randtime_T[0],randtime_T[1])
        dict_cars[id]=Car(id,state,fuel,fuel_threshold,waiting_time_threshold)
        cars_onroad.append(id)
    return len(dict_cars)


# Ensure that the fuel value is always equal or more than 0
def reduce_fuel(car:Car,value:int)->int:
    """
    Decrementing the fuel of the car only if in the state "onroad".
    :param car:
    :return:
    """
    """
    >>> reduce_fuel(Car(2,"stopped",2,5,5), 1)
    1
    """

    if value <= car.get_fuel():
        car.reduce_fuel(value)
    return car.get_fuel()

# Ensure that a car that increments the waiting_time is in the state "fuelstation".
def increment_waiting_time(car:Car,value)->bool:
    """
    Incrementing waiting time only for cars in state "fuelstation". Return true if time is incremented.
    :param car:
    :param value:
    :return:
    >>> increment_waiting_time(Car(2,"stopped",2,5,5), 1)
    False
    """
    initial = car.get_waiting_time()
    if car.get_state() == "fuelstation":
        car.increment_waiting_time(value)
    final = car.get_waiting_time()
    if initial < final:
        return True
    else:
        return False

# Ensure that if a car is on the road it'll be queued only if it has the fuel level less or equal than the threshold.
def set_car_queue(car:Car,queue:list,onroad:list)->bool:
    """
    Add a car to a queue and reset waiting time. Return true if the car is added to the queue.
    :param car:
    :return:
   # >>> set_car_queue(Car(2,"onroad",2,5,5), cars_queue, cars_onroad)
   #True

   #>>> set_car_queue(Car(2,"onroad",6,7,5), cars_queue, cars_onroad)
   #False
    """
    if car.get_state() == "onroad":
        if car.get_fuel() < car.get_fuel_threshold():
            car.set_state("fuelstation")
            car.reset_waiting_time()
            queue.append(car.get_id())
            onroad.remove(car.get_id())
            return True
    return False
# Ensure that the car ID matches the first on the list.
# Ensure that the car is in the "fuelstation" state.
def fill_fuel_tank(car:Car,queue:list,onroad:list)->bool:
    """
    Fill the tank if the car is the first in the queue. Return true if the car fills the tank and returns to road.
    :param car:
    :param val:
    :return:

    >>> fill_fuel_tank(Car(2,"fuelstation",5,5,5), [0], [])
    True
    >>> fill_fuel_tank(Car(2,"onroad",5,5,5), [0], [])
    False
    """
    if len(queue) > 0:
        if car.get_id() == queue[0]:
            car.set_state("onroad")
            car.set_fuel(10)
            queue.remove(car.get_id())
            onroad.append(car.get_id())
            return True
    return False
# Ensure that the car is out of fuel only if fuel is 0
# Ensure that the car is in the "onroad" state.
def onroad_out_of_fuel(car:Car,cars_onroad:list,cars_stopped:list)->bool:
    """
    Test if the car is out of fuel. In that case, add car to stopped list.
    Returns true if car is added to stopped car list.
    :param car:
    :param cars_onroad: cars' id on the road
    :param cars_stopped: cars' id stopped without fuel
    :return: return true if fuel is zero and car is on the road
    >>> onroad_out_of_fuel(Car(2,"stopped",0,5,5),[2],[])
    False
    >>> onroad_out_of_fuel(Car(3,"onroad",0,5,5),[3],[])
    True
    >>> onroad_out_of_fuel(Car(3,"onroad",5,5,5),[3],[])
    False
    """
    if car.get_fuel()==0:
    # if car.get_state()=="onroad":
    # if car.get_fuel()==0 and car.get_state()=="onroad":
        car.set_state("stopped")
        cars_onroad.remove(car.get_id())
        cars_stopped.append(car.get_id())
        return True
    return False

def remove_car_queue(car:Car,cars_queue:list,cars_onroad:list)->bool:
    """

    :param car:
    :param cars_queue:
    :param cars_onroad:
    :return:

    >>> remove_car_queue(Car(2, "onroad", 5, 7, 5), [], [])
    False
    """

    if car.get_waiting_time() > car.get_waiting_time_threshold():
        car.set_state("onroad")
        cars_queue.remove(car.get_id())
        cars_onroad.append(car.get_id())
        return True
    return False


def print_car_state(car:Car):
    """
    Print the state of the car.
    :param car:
    :return:
    """
    print("Id=",car.get_id()," Fuel=",car.get_fuel()," Fuel_Thr=",car.get_fuel_threshold(),"Waiting_Time=",car.get_waiting_time(),"Waiting_Thr=",car.get_waiting_time_threshold())

def main():
    # Test using doctest
    doctest.testmod()
    # Initialization: A car is a member of the dictionary with a key (id) and the list.
    cars = dict()
    res = create_dict_cars(cars, 6, (5,7), (2,5), (2,4), cars_onroad)
    print("Number of cars created:",res)
    # Cycle
    tick = 0
    while tick < MAX_TICKS:
        for id in cars:
            car = cars[id]
            reduce_fuel(car,1)
            onroad_out_of_fuel(car,cars_onroad,cars_stopped)
            set_car_queue(car,cars_queue,cars_onroad)
            increment_waiting_time(car,1)
            fill_fuel_tank(car,cars_queue,cars_onroad)
            remove_car_queue(car,cars_queue,cars_onroad)
            #print_car_state(car)

        # Print State
        if tick % 1 == 0:
            nr_stopped = len(cars_stopped)
            nr_onroad = len(cars_onroad)
            nr_queue = len(cars_queue)
        mean_fuel = 0
        if len(cars_onroad) > 0:
            for id in cars_onroad:
                mean_fuel += cars.get(id).get_fuel()
                mean_fuel = mean_fuel / len(cars_onroad)
        '''
        print("*******************")
        print("tick:",tick)
        print("Stopped:",nr_stopped)
        print("Onroad:",nr_onroad)
        print("Queue:",nr_queue)
        print("Mean fuel:",mean_fuel)
        print("*******************")
        '''
        tick += 1

main()