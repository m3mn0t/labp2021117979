class HeapInterface:
    """Interface for all heaps."""

    # Constructor
    def __init__(self, source_collection=None):
        """
        Sets the initial state of self, which includes the
        contents of source_collection, if it's present
        :param source_collection:
        """
        self.items: list = []
        if source_collection:
            for item in source_collection:
                self.add(item)

    # collection accessor methods
    def is_empty(self) -> bool:
        """
        Tests if self is empty.
        :return: True if len(self) is 0, otherwise False
        """
        return True

    def __len__(self) -> int:
        """
        Gets the number of items in self.
        :return: the number of items in self
        """
        return len(self.items)

    def __str__(self) -> str:
        """
        Builds the string representation of self.
        :return: String representation of self
        """
        return str(self.items)

    def __iter__(self):
        """
        Supports a preorder traversal on a view of self.
        :return: an iteration of self
        """
        return None

    # collection mutator methods

    def clear(self) -> None:
        """
        Makes self become empty.
        :return: None
        """
        pass

    # Heap accessor methods

    def peek(self):
        """

        :return:
        """

    def add(self, item) -> None:
        """
        Adds item to self
        :param item: the item to insert
        :return: None
        """
        self.items.append((item))
        pos: int = len(self.items) - 1
        stop: bool = False
        while pos > 0 and not stop:
            pos_parent: int = (pos - 1) // 2
            if self.items[pos] <= self.items[pos_parent]:
                #trocar
                self.items[pos] = self.items[pos_parent]
                self.items[pos_parent] = item
                pos = pos_parent
            else:
                stop = True

    def pop(self):
        """
        Removes the root of self, assuming the self is not empty
        :return: the item removed
        """
        if not self.is_empty():
            root = self.items[0]
            if len(self) == 1:
                self.items.pop()
            else:
                # existe 2 ou mais elementos
                self.items[0] = self.items[len(self) - 1]
                self.items.pop()
                stop = False
                pos = 0
                while not stop:
                    pos_left_child = pos * 2 + 1
                    pos_right_child = pos * 2 + 2
                    if pos_right_child < len(self): # existe esquerdo e direito
                        if self.items[pos] > self.items[pos_left_child] or \
                               self.items[pos] > self.items[pos_right_child]:
                            if self.items[pos_left_child] < self.items[pos_right_child]:
                                self.items[pos_left_child], self.items[pos] = \
                                    self.items[pos_left_child], self.items[pos]
                                pos = pos_right_child
                            else:
                                self.items[pos_right_child], self.items[pos] = \
                                    self.items[pos_right_child], self.items[pos]
                                pos = pos_right_child