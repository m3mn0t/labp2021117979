import datetime
import random


class Fila:
    """Implementação do TDA Fila baseada em List"""

    def __init__(self, source_collection=None):
        """ Define o estado inicial de self com sourceCollection """
        self._itens = list()
        self._size = 0
        if source_collection:
            for item in source_collection:
                self.add(item)

    # metodos gerais de coleção
    def is_empty(self):
        """ Retorna True se len(self) é 0, senão False """
        return len(self) == 0

    def __len__(self):
        """ Retorna o numero de elementos da queue """
        return self._size

    def __str__(self):
        """ Retorna a representação em string de self """
        return str(self._itens) + " :  " + str(self._size) + " elementos"

    def clear(self):
        """ Torna o self vazio"""
        self._itens = list()
        self._size = 0

    def __iter__(self):
        """ Suporta a iteração sobre self """
        return None

    # metodos especificos da fila

    def peek(self):
        """ Retorna o item que está na frente de self.
             precondição: self não é vazio."""
        if self.is_empty():
            raise KeyError(" queue is empty")
        else:
            return self._itens[0]

    def add(self, item):
        """ Acrescenta item a self no fim
            pos-condição: item foi acrescentado a self """
        self._itens.append(item)
        self._size += 1

    def pop(self):
        """ Remove elemento do topo de self e retorna esse elemento.
             precondição: self não é vazio.
             pos-condição: item foi removido de self """
        if self.is_empty():
            raise KeyError(" lista is empty")
        else:
            self._size -= 1
            return self._itens.pop(0)


class Pilha:
    """ Implementação do TDA Pilha baseada em list """

    def __init__(self, source_collection=None):
        """Define o estado inicial de self com sourceCollection """
        self._items = []
        self._size = 0

    # metodos gerais de coleção

    def is_empty(self):
        """ Retorna True se len(self) é 0, senão False """
        return self._size == 0

    def __len__(self):
        """ Retorna o numero de elementos do stack """
        return self._size

    def __str__(self):
        """ Retorna a representação em string de self """
        return str(self._items) + " :  " + str(self._size) + " elementos"

    def clear(self):
        """ Torna self vazio"""
        pass

    def __iter__(self):
        """ Suporta a iteração sobre self """
        return None

    # metodos específicos da pilha

    def peek(self):
        """ Retorna o item que está no topo de self.
             precondição: self não é vazio."""
        if not self.is_empty():
            return self._items[self._size - 1]  # + à direita
        else:
            raise KeyError(" pilha vazia!")

    def push(self, item):
        """ Sobrepoe item a self
            pos-condição: item foi sobreposto a self """
        self._items.append(item)
        self._size += 1

    def pop(self):
        """ Remove elemento do topo de self e retorna esse elemento.
             precondição: self não é vazio.
             pos-condição: topo foi removido de self """
        if len(self) > 0:
            self._size -= 1
            return self._items.pop()
        else:
            raise KeyError(" pilha vazia!")


class Veiculos:
    def __init__(self):
        self.hora_acont = int
        self.actividade = int
        self.horadeentrada = int


class Maquina:
    def __init__(self):
        self.hora_livre = int


has = 0  # Hora Atual de Simulação
faf = Fila()  # fila de acontecimentos futuros
espera = Fila()
livre = Pilha()
"""dt = datetime.datetime.now()
ts = dt.timestamp()
"""
tts = int(input("Qual o Tempo Total de Simulação?"))
"""nms = input("Qual o numero de maquinas a simular?")
print(dt.hour, ':', dt.minute)
print(ts) """
duracao_atividade = random.randint(15, 26)
intervalo_chegada = range(15, 60)

v1 = Veiculos()
v1.hora_acont = 3
faf.add(v1.hora_acont)
livre.push(1)
print(livre)

#print(netxv1)
print(espera)
print(faf)

#  Atividade = 1 fim de lavagem, 0 chegada
while tts != has:
    # ENTRADA
    espera.add(faf.pop())  # retira veiculo da faf e coloca o valor na fila espera
    v2 = Veiculos()  # Cria veiculo novo e coloca na faf
    v2.hora_acont = duracao_atividade
    faf.add(v2.hora_acont)
    print(len(espera))
    if espera.is_empty() == False and livre.is_empty() == False:
        espera.pop()
        livre.pop()
        v1.actividade = 1




