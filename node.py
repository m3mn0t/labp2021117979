class Node:
    """
    represents a linked node

    """
    def __init__(self, data, next=None):
        """
        sets the initial state od self (a node)
        :param data: the value
        :param next: the next node
        :return:
        """
        self.data = data
        self.next = next
    def __str__(self):
        """

        :return:
        """
        return str(self.data)
