#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from ID3_classification import DecisionTreeClassifier
# define features e atributo de decisão
data = {
    'vento': ['N', 'S', 'E', 'O'],
    'maré': ['preia_mar', 'baixa_mar'],
    'ondulação': ['mar_flat', 'média', 'elevada'],
    'boas_ondas': ['Sim', 'Não']
}

# cria um dataFrame (tabela) vazia
dadosSurf = pd.DataFrame(columns=data.keys())

# estabelece a semente para evitar que os dados sejam renovados de cada vez que corre
np.random.seed(13)

for i in range(100):
    dadosSurf.loc[i, 'vento'] = str(np.random.choice(data['vento'], 1)[0])
    dadosSurf.loc[i, 'maré'] = str(np.random.choice(data['maré'], 1)[0])
    dadosSurf.loc[i, 'ondulação'] = str(np.random.choice(data['ondulação'], 1)[0])
    dadosSurf.loc[i, 'boas_ondas'] = str(np.random.choice(data['boas_ondas'], 1)[0])

# preparar os dados 
X = dadosSurf [['vento', 'maré', "ondulação"]].values.tolist()
labels = dadosSurf['boas_ondas'].values.tolist()
feature_names = ['vento', 'maré', 'ondulação']



arvore = DecisionTreeClassifier(X, feature_names, labels)
# corre o algoritmo
arvore.id3()

# apresenta a árvore resultante


def prinTree (raiz, prof):
    if not raiz.childs : 
        for i in range(prof):
            print ("   ", end="")    
        print ("=> boas_ondas == ", raiz.value)
        return
    n = len(raiz.childs)
    if n>0:
        for i in range(n):
            for j in range(prof):
                print ("   ", end="")
            if prof>0 : print ("AND ", end="")
            print(raiz.value," == ",raiz.childs[i].value)
            prinTree (raiz.childs[i].next, prof+1)
        
prinTree(arvore.node, 0)
