#!/usr/bin/env python
# coding: utf-8
import math


class Node:

    def __init__(self):
        self.value = None
        self.next = None
        self.childs = None


class DecisionTreeClassifier:
    """Classe com o algoritmo ID3."""

    def __init__(self, X, feature_names, labels):
        self.X = X  # tabela de dados apenas com as features (os labels são trabalhados à aprte)
        self.feature_names = feature_names  # lista de names das features
        self.labels = labels  # coluna de rótulos (labels)
        self.labelCategories = list(set(labels))  # valores do domínio dos labels
        # numero de exemplos para cada labelCategories
        self.labelCategoriesCount = [list(labels).count(x) for x in self.labelCategories]
        self.node = None  # primeiro nó com acesso aos restantes
        # calculo da entropia do nó raiz
        self.entropy = self._get_entropy([x for x in range(len(self.labels))])

    def _get_entropy(self, x_ids):  # cálculo da entropia

        labels = [self.labels[i] for i in x_ids]

        label_number = [labels.count(x) for x in self.labelCategories]

        entropy = sum([-count / len(x_ids) * math.log(count / len(x_ids), 2)
                       if count else 0
                       for count in label_number
                       ])

        return entropy

    def _get_information_gain(self, x_ids, feature_id):  # Cálculo do ganho de informação

        info_gain = self._get_entropy(x_ids)   # Entropia total

        x_features = [self.X[x][feature_id] for x in x_ids]

        feature_vals = list(set(x_features))

        feature_v_count = [x_features.count(x) for x in feature_vals]

        feature_v_id = [
            [x_ids[i]
             for i, x in enumerate(x_features)
             if x == y]
            for y in feature_vals
        ]

        # compute the information gain with the chosen feature
        info_gain_feature = sum([v_counts / len(x_ids) * self._get_entropy(v_ids)
                                 for v_counts, v_ids in zip(feature_v_count, feature_v_id)])

        info_gain = info_gain - info_gain_feature

        return info_gain

    def _get_feature_max_information_gain(self, x_ids,
                                          feature_ids):  # identifica atributo com maior ganho de informação
        f_entropy = [self._get_information_gain(x_ids, feature_id) for feature_id in feature_ids]

        max_id = feature_ids[f_entropy.index(max(f_entropy))]

        return self.feature_names[max_id], max_id

    def id3(self):  # inicializa e chama a função recursiva
        x_ids = [x for x in range(len(self.X))]  # atribui índice inteiro a cada exemplo
        feature_ids = [x for x in range(len(self.feature_names))]  # atribui um índice inteiro a cada feature
        self.node = self._id3_recv(x_ids, feature_ids,
                                   self.node)  # cria recursivamente toda a árvore começando pelo nó raiz

    def _id3_recv(self, x_ids, feature_ids, node):  # ID3 reursivo
        if not node:
            node = Node()  # inicializa Node caso não tenha sido feito antes
        # ordena os índices do atributo de decisão pela mesma ordem da tabela de exemplos
        labels_in_features = [self.labels[x] for x in x_ids]
        # se todos os exemplos têm o mesmo label (nó puro), return node
        if len(set(labels_in_features)) == 1:
            node.value = self.labels[x_ids[0]]
            return node
        # se já não há mais feaures, retorna o nó com o label mais frequente
        if len(feature_ids) == 0:
            node.value = max(set(labels_in_features), key=labels_in_features.count)  # compute mode
            return node
        # escolher a feature com o maior ganho de informação
        best_feature_name, best_feature_id = self._get_feature_max_information_gain(x_ids, feature_ids)
        node.value = best_feature_name
        node.childs = []
        # obter a lista de valores do domínio dessa feature
        feature_values = list(set([self.X[x][best_feature_id] for x in x_ids]))
        # para cada um dos valores do domínio
        for value in feature_values:
            child = Node()  # instancia um novo nó
            child.value = value  # identifica o nó com o nome do valor do domínio (classe)
            node.childs.append(child)  # junta o novo nó à lista de nós filhos
            child_x_ids = [x for x in x_ids if
                           self.X[x][best_feature_id] == value]  # determina os exemplos do novo nó child
            if not child_x_ids:
                child.next = max(set(labels_in_features), key=labels_in_features.count)
                print('')
            else:
                if feature_ids and best_feature_id in feature_ids:
                    to_remove = feature_ids.index(best_feature_id)
                    feature_ids.pop(to_remove)
                # chamada recursiva
                child.next = self._id3_recv(child_x_ids, feature_ids, child.next)
        return node
