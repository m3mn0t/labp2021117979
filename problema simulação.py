
import random


class Car:
    def __init__(self, id, state, fuel, fuel_threshold, waiting_time_threshold):
        '''inicia o estado do automóvel'''
        self.__id = id
        self.__state = state
        self.__fuel = fuel
        self.__fuel_threshod = fuel_threshold
        self.__waiting_time_threshold = waiting_time_threshold
        self.__nr_times_station = 0
        self.__waiting_time = 0

    def print_state(self):
        print("-------------")
        print("Car:", self.__id)
        print("State:", self.__state)
        print("Fuel:", self.__fuel)
        print("Waiting time threshold:", self.__waiting_time_threshold)
        print("Nr. times in station:", self.__nr_times_station)
        print("Waiting time:", self.__waiting_time)
        print("-------------")

    def reset_waiting_time(self):
        '''leva para zero o valor de espera'''
        self.__waiting_time = 0

    def increment_waiting_time(self, value):
        '''incrementa o valor de espera na fila'''
        self.__waiting_time += value

    def get_waiting_time(self):
        '''retorna o tempo de espera na fila de combustível'''
        return self.__waiting_time

    def get_waiting_time_threshod(self):
        '''retorna o tempo limite de espera na fila de combustível'''
        return self.__waiting_time_threshold

    def get_id(self):
        '''retorna o id do automóvel'''
        return self.__id

    def get_state(self):
        '''retorna o estado'''
        return self.__state

    def set_state(self, state):
        '''atribuí um estado ao automóvel'''
        self.__state = state

    def reduce_fuel(self, value):
        '''reduz o nível de combustível por um valor value'''
        self.__fuel -= value

    def get_fuel(self):
        '''retorna o nível de combustível'''
        return self.__fuel

    def set_fuel(self, fuel):
        '''atribui um valor ao nível de combustível'''
        self.__fuel = fuel

    def get_fuel_threshold(self):
        '''retorna o limite mínimo de combustível'''
        return self.__fuel_threshod

    def increment_nr_times_station(self):
        '''incrementa o número de vez que usou o posto de combustível'''
        self.__nr_times_station += 1

    def get_r_times_station(self):
        return self.__nr_times_station

    def imprime(self):
        print("** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** *")
        print("Estad o final")
        print("Nr.Ticks: 1000")
        print("Nr.Agentes: 14")
        print("Estacao de servico: 2")
        print("Estrada: 6")
        print("Parados: 4")
        print("Combustivel (medio):4.5")
        print("Nr.de vezes na estacao(medio):0.3")
        print("** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** *")


def main():
    # Inicializar as estruturas de dados | Start the data structures
    cars_stopped = [] # The ID of cars stopped
    cars_onroad = [] # The ID of cars on road
    cars_station = [] # The ID of cars on station
    cars = dict()

    for i in range(0, 10):
        fuel = random.randint(6, 10)
        fuel_threshold = random.randint(2, 6)
        waiting_time_threshold = random.randint(2, 10)

        car = Car(i, "onroad", fuel, fuel_threshold, waiting_time_threshold)
        cars[i] = car

        #Append da lista de carros na estrada
        cars_onroad.append(i)
        #Append da lista de carros parados
        cars_stopped.append(i)

    for i in cars_onroad:
        print(cars[i].print_state())

    #Imprime os carros na estrada
    print(cars_onroad)

    #Ciclo
    max_ticks = 10
    tick = 0
    while tick <= max_ticks:
        for i in cars_onroad:
            cars[i].reduce_fuel(1)
            tick = tick + 1

            #Atualização dos agentes por cada ciclo
            car = cars.get(i)
            car.get_state()
            if car.get_fuel() < car.get_fuel_threshold():
                car.set_state("fuelstation")
                cars_station.append(i)
                cars_onroad.remove(i)

    #Imprime informações em relação ao tempo e o combustível
    print('O combustível do carro 4 é:', cars[4].get_fuel())
    print('O tempo de espera do carro 8 é:', cars[7].get_waiting_time())


main()
